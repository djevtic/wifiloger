package com.example.wifiloger.fragment;

import java.util.ArrayList;

import org.json.JSONObject;

import com.example.wifiloger.NetworkArrayAdapter;
import com.example.wifiloger.NetworkDataHolder;
import com.example.wifiloger.R;
import com.example.wifiloger.model.Network;
import com.example.wifiloger.utils.FileUtils;
import com.example.wifiloger.utils.JsonUtils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class ListViewFragment extends ListFragment{
    
    private ListView mListview;
    private NetworkArrayAdapter mAdapter;
    
    private FileUtils mFileUtils;
    private JsonUtils mJsonUtils;
    private NetworkDataHolder mDataHolder;
    private  ArrayList<Network> mNetworkList;
    
    @Override
    public View onCreateView(LayoutInflater inflater,
       ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_main, container, false);
        mDataHolder = NetworkDataHolder.getInstance();
       /**
        * Inflate the layout for this fragment
        */
       return view;
    }
    
    @Override
    public void onActivityCreated (Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        mFileUtils = new FileUtils(getActivity().getApplicationContext());
        mJsonUtils = new JsonUtils(getActivity().getApplicationContext());
        
        String fromFile = mFileUtils.readFromFile();
        if (fromFile != null) {
            Log.d("djevtic","Got from File: "+fromFile);
            JSONObject jsonObject = mJsonUtils.stringToJson(fromFile);
            mDataHolder.setmNetworkList(mJsonUtils.jsonToArrayList(jsonObject));
        }
        
        mListview = getListView();
        mNetworkList = new ArrayList<Network>();
        mNetworkList = mDataHolder.getmNetworkList();
        mAdapter = new NetworkArrayAdapter(getActivity().getApplicationContext(),
                R.layout.network_list_view, mNetworkList);
        setListAdapter(mAdapter);
        
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver,
            new IntentFilter("list-update"));
    }
    
    @Override
    public void onDestroyView (){
        super.onDestroyView();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
    }
    
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
      @Override
      public void onReceive(Context context, Intent intent) {
          mNetworkList = mDataHolder.getmNetworkList();
          mAdapter.setNetworkList(mNetworkList);
          mAdapter.notifyDataSetChanged();
      }
    };

}
