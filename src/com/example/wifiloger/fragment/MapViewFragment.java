package com.example.wifiloger.fragment;

import java.util.ArrayList;

import com.example.wifiloger.NetworkDataHolder;
import com.example.wifiloger.R;
import com.example.wifiloger.model.Network;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class MapViewFragment extends Fragment{
    
    private  ArrayList<Network> mNetworkList;
    private NetworkDataHolder mDataHolder;
    
    @Override
    public View onCreateView(LayoutInflater inflater,
       ViewGroup container, Bundle savedInstanceState) {
       /**
        * Inflate the layout for this fragment
        */
       return inflater.inflate(
               R.layout.map_view, container, false);
    }
    
    @Override
    public void onActivityCreated (Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        // Operations with Google Map
        mDataHolder = NetworkDataHolder.getInstance();
        mNetworkList = mDataHolder.getmNetworkList();
        GoogleMap map = ((MapFragment)getActivity().getFragmentManager().findFragmentById(R.id.map)).getMap();
        for(int i = 0;mNetworkList.size() > i;i++){
            LatLng storeLocation = new LatLng(mNetworkList.get(i).getLatitude(), mNetworkList.get(i).getLongitude());
            map.addMarker(new MarkerOptions().position(storeLocation).title(mNetworkList.get(i).getSSID()));
        }
        LocationManager locationManager;
        locationManager = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        LatLng storeLocation = new LatLng(location.getLatitude(), location.getLongitude());
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(storeLocation, 15));
    }

}
