
package com.example.wifiloger.services;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

import com.example.wifiloger.MainActivity;
import com.example.wifiloger.NetworkDataHolder;
import com.example.wifiloger.R;
import com.example.wifiloger.R.drawable;
import com.example.wifiloger.R.string;
import com.example.wifiloger.model.Network;
import com.example.wifiloger.utils.FileUtils;
import com.example.wifiloger.utils.JsonUtils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

public class LogerService extends Service {

    private WifiManager mWifi;

    private List<ScanResult> mResults;

    protected int mSize;

    String ITEM_KEY = "key";

    Thread mThread;

    private NotificationManager mNM;

    private HashMap<String, Network> mHash;

    private FileUtils mFileUtils;

    private JsonUtils mJsonUtils;

    private BroadcastReceiver mReceiver;

    private IntentFilter mIntentFilter;

    private ArrayList<Network> mNetworkArray;

    private int NOTIFICATION = R.string.local_service_started;
    
    public static boolean mIsRuning = false;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mIsRuning = true;
        mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        prepareForScaning();
        showNotification();
        startScanThread();
        return START_STICKY;
    }

    private void stopScanThread() {
        mNM.cancel(NOTIFICATION);
        mThread.interrupt();
        unregisterReceiver(mReceiver);

    }

    private void startScanThread() {
        receiverRegistration();
        mThread = new Thread() {
            @Override
            public void run() {
                try {
                    while (true) {
                        sleep(3000);
                        startScan();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        mThread.start();
    }

    private void receiverRegistration() {
        mWifi = (WifiManager)getSystemService(Context.WIFI_SERVICE);
        if (mWifi.isWifiEnabled() == false) {
            mWifi.setWifiEnabled(true);
        }
        mReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                mResults = mWifi.getScanResults();
                mSize = mResults.size();
            }
        };
        mIntentFilter = new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        registerReceiver(mReceiver, mIntentFilter);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mIsRuning = false;
        stopScanThread();
        onScanFinished();
        Toast.makeText(this, getText(R.string.local_service_stoped), Toast.LENGTH_LONG).show();
    }

    private void startScan() {
        mWifi.startScan();
        try {
            mSize = mSize - 1;
            while (mSize >= 0) {
                if (mHash.get(mResults.get(mSize).SSID) == null) {
                    fillUpHashMap(mResults.get(mSize));
                }
                mSize--;
            }
            mNetworkArray = mJsonUtils.jsonToArrayList(mJsonUtils.createJsonFromHash(mHash));
            NetworkDataHolder.getInstance().setmNetworkList(mNetworkArray);
            sendMessageToList();
        } catch (Exception e) {
        }
    }

    private void prepareForScaning() {
        mHash = new HashMap<String, Network>();
        mFileUtils = new FileUtils(getApplicationContext());
        mJsonUtils = new JsonUtils(getBaseContext());

        String fromFile = mFileUtils.readFromFile();
        if (fromFile != null) {
            JSONObject jsonObject = mJsonUtils.stringToJson(fromFile);
            mHash = mJsonUtils.jsonToHashMap(jsonObject);
        }
    }

    private void onScanFinished() {
        JSONObject jsonObject = mJsonUtils.createJsonFromHash(mHash);
        mFileUtils.writeToFile(jsonObject.toString());
    }

    private void fillUpHashMap(ScanResult result) {
        LocationManager locationManager;
        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        int level = WifiManager.calculateSignalLevel(result.level, 10);
        Network networkCheck = mHash.get(result.BSSID);
        Network network = new Network(result.BSSID, result.SSID, result.frequency,
                result.capabilities, location.getLongitude(), location.getLatitude(), level);
        if (networkCheck == null) {
            mHash.put(result.BSSID, network);
        } else if (networkCheck.getLevel() < level) {
            mHash.put(result.BSSID, network);
        }
    }

    /**
     * Show a notification while this service is running.
     */
    private void showNotification() {
        CharSequence text = getText(R.string.local_service_started);
        Notification notification = new Notification(R.drawable.ic_action_refresh, text,
                System.currentTimeMillis());
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                MainActivity.class), 0);
        notification.setLatestEventInfo(this, getText(R.string.app_name), text, contentIntent);
        mNM.notify(NOTIFICATION, notification);
    }

    private void sendMessageToList() {
        Intent intent = new Intent("list-update");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

}
