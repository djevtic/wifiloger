
package com.example.wifiloger;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.wifiloger.model.Network;

public class NetworkArrayAdapter extends ArrayAdapter<Network> {
    
    private List<Network> mNetwork;

    public NetworkArrayAdapter(Context context, int resource, List<Network> network) {
        super(context, resource, network);
        this.mNetwork = network;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(mNetwork.size()>position){
        Network network = mNetwork.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.network_list_view,
                    parent, false);
        }
        TextView ssid = (TextView)convertView.findViewById(R.id.network_ssid);
        TextView bssid = (TextView)convertView.findViewById(R.id.network_bssid);
        TextView capabilities = (TextView)convertView.findViewById(R.id.network_capabilities);
        TextView frequency = (TextView)convertView.findViewById(R.id.network_frequency);

        ssid.setText(network.getSSID());
        bssid.setText(network.getBSSID());
        capabilities.setText(network.getCapabilities());
        frequency.setText(String.valueOf(network.getFrequency()));
        }
        return convertView;
    }
    
    @Override
    public int getCount() {
        return mNetwork.size();
    }
    
    public void setNetworkList(List<Network> network) {
        this.mNetwork = network;
    }

}
