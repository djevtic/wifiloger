package com.example.wifiloger.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.wifiloger.model.Network;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

public class JsonUtils {

	private Context mContext;

	private final static String BASE = "base";
	private final static String SSID = "ssid";
	private final static String FREQUENCY = "frequency";
	private final static String BSSID = "bssid";
	private final static String CAPABILITIES = "capabilities";
	private final static String LONGITUDE = "longitude";
	private final static String LATITUDE = "latitude";
	private final static String LEVEL = "level";

	public JsonUtils(Context context) {
		this.mContext = context;
	}

	public JSONObject createJsonFromHash(HashMap<String, Network> hash) {

		JSONObject base = new JSONObject();
		JSONArray array = new JSONArray();
		try {
		    Iterator<Entry<String, Network>> it = hash.entrySet().iterator();
			while (it.hasNext()) {
				JSONObject jsonnetwork = new JSONObject();
				Map.Entry pairs = (Map.Entry) it.next();
				Network network = (Network) pairs.getValue();
				jsonnetwork.put(SSID, network.getSSID());
				jsonnetwork.put(FREQUENCY, network.getFrequency());
				jsonnetwork.put(BSSID, network.getBSSID());
				jsonnetwork.put(CAPABILITIES, network.getCapabilities());
				jsonnetwork.put(LONGITUDE, network.getLongitude());
				jsonnetwork.put(LATITUDE, network.getLatitude());
				jsonnetwork.put(LEVEL, network.getLevel());
				array.put(jsonnetwork);
				//it.remove(); // avoids a ConcurrentModificationException
			}

			base.put(BASE, array);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return base;
	}

	public HashMap<String, Network> jsonToHashMap(JSONObject base) {
		HashMap<String, Network> map = new HashMap<String, Network>();
		try {
			JSONArray array = base.getJSONArray(BASE);
			for (int i = 0; i < array.length(); i++) {
				String ssid = array.getJSONObject(i).getString(SSID);
				String bssid = array.getJSONObject(i).getString(BSSID);
				int frequency = array.getJSONObject(i).getInt(FREQUENCY);
				String capability = array.getJSONObject(i).getString(
						CAPABILITIES);
				double longitude = array.getJSONObject(i).getDouble(LONGITUDE);
				double latitude = array.getJSONObject(i).getDouble(LATITUDE);
				int level = array.getJSONObject(i).getInt(LEVEL);
				Network network = new Network(bssid, ssid, frequency,
						capability, longitude, latitude, level);
				map.put(ssid, network);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return map;
	}

	public ArrayList<Network> jsonToArrayList(JSONObject base) {
		ArrayList<Network> list = new ArrayList<Network>();
		JSONArray array;
		try {
			array = base.getJSONArray(BASE);

			for (int i = 0; i < array.length(); i++) {
				String ssid = array.getJSONObject(i).getString(SSID);
				String bssid = array.getJSONObject(i).getString(BSSID);
				int frequency = array.getJSONObject(i).getInt(FREQUENCY);
				String capability = array.getJSONObject(i).getString(
						CAPABILITIES);
				double longitude = array.getJSONObject(i).getDouble(LONGITUDE);
				double latitude = array.getJSONObject(i).getDouble(LATITUDE);
				int level = array.getJSONObject(i).getInt(LEVEL);
				
				Network network = new Network(bssid, ssid, frequency,
						capability, longitude, latitude, level);
				list.add(network);
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	
	public JSONObject stringToJson(String jsonString){
		try {
			JSONObject base = new JSONObject(jsonString);
			return base;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

}
