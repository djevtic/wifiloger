package com.example.wifiloger.utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import android.content.Context;
import android.util.Log;

public class FileUtils {
	
	private Context mContext;
	private static final String FILE_NAME = "logger.txt";
	
	public FileUtils(Context context){
		this.mContext = context;
	}
	
	public void writeToFile(String data) {
	    try {
	        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(mContext.openFileOutput(FILE_NAME, Context.MODE_PRIVATE));
	        outputStreamWriter.write(data);
	        outputStreamWriter.close();
	    }
	    catch (IOException e) {
	        Log.e("djevtic", "File write failed: " + e.toString());
	    } 
	}


	public String readFromFile() {

	    String ret = null;

	    try {
	        InputStream inputStream = mContext.openFileInput(FILE_NAME);

	        if ( inputStream != null ) {
	            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
	            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
	            String receiveString = "";
	            StringBuilder stringBuilder = new StringBuilder();

	            while ( (receiveString = bufferedReader.readLine()) != null ) {
	                stringBuilder.append(receiveString);
	            }

	            inputStream.close();
	            ret = stringBuilder.toString();
	        }
	    }
	    catch (FileNotFoundException e) {
	        Log.e("djevtic", "File not found: " + e.toString());
	    } catch (IOException e) {
	        Log.e("djevtic", "Can not read file: " + e.toString());
	    }

	    return ret;
	}

}
