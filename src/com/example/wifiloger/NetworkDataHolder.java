package com.example.wifiloger;

import java.util.ArrayList;

import com.example.wifiloger.model.Network;

public class NetworkDataHolder {
    
    private static NetworkDataHolder instance = null;
    
    private  ArrayList<Network> mNetworkList;
    
    protected NetworkDataHolder() {
        mNetworkList = new ArrayList<Network>();
       // Exists only to defeat instantiation.
    }
    public static NetworkDataHolder getInstance() {
       if(instance == null) {
          instance = new NetworkDataHolder();
       }
       return instance;
    }
    /**
     * @return the mNetworkList
     */
    public synchronized  ArrayList<Network> getmNetworkList() {
        return mNetworkList;
    }
    /**
     * @param mNetworkList the mNetworkList to set
     */
    public synchronized  void setmNetworkList(ArrayList<Network> mNetworkList) {
        this.mNetworkList = mNetworkList;
    }
    
    synchronized void addElementToNetworkList(Network network) {
            ArrayList<Network> networkList = getmNetworkList();
            networkList.add(network);
            setmNetworkList(networkList);
        }

}
